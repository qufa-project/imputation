from ..lib.data_imputation import MVImputer
from ..lib.outlier_detection import OutlierDetector
import os
import boto3
import pandas as pd
import io
import json

config = None
with open('./configs/config.json') as json_file:
    config = json.load(json_file)

client = boto3.client(
    's3',
    region_name=os.environ.get('REGION_NAME') or config['region_name'],
    aws_access_key_id=os.environ.get(
        'AWS_ACCESS_KEY_ID') or config['aws_access_key_id'],
    aws_secret_access_key=os.environ.get(
        'AWS_SECRET_ACCESS_KEY') or config['aws_secret_access_key']
)


def threaded_imputation_task(filename):
    data = get_obj_from_s3(filename)
    mvi = MVImputer(input_fname=filename, data=data,
                    result_path='imputation_temp')
    data, jsonObj = mvi.runWithoutSaveFile()

    filenameForUpload = removeExtensionFromFilename(filename=filename)
    uploadDfToS3(data, 'imputation/impute/'+filenameForUpload+'/result.csv')
    uploadJsonToS3(jsonObj, 'imputation/impute/' +
                   filenameForUpload+'/result.json')


def threaded_outlier_task(filename):
    data = get_obj_from_s3(filename)
    detector = OutlierDetector(
        input_fname=filename, data=data, result_path='outlier_temp')
    data, jsonObj = detector.runWithoutSaveFile()

    filenameForUpload = removeExtensionFromFilename(filename=filename)
    uploadDfToS3(data, 'imputation/outlier/'+filenameForUpload+'/result.csv')
    uploadJsonToS3(jsonObj, 'imputation/outlier/' +
                   filenameForUpload+'/result.json')


def get_obj_from_s3(input_fname):
    obj = client.get_object(Bucket='qufa-test', Key=input_fname)
    data = pd.read_csv(io.BytesIO(obj['Body'].read()))
    return data


def uploadDfToS3(data, key):
    with io.StringIO() as csv_buffer:
        data.to_csv(csv_buffer, index=False)

        response = client.put_object(
            Bucket='qufa-test', Key=key, Body=csv_buffer.getvalue()
        )

        status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")

        if status == 200:
            print(f"Successful S3 put_object response. Status - {status}")
        else:
            print(f"Unsuccessful S3 put_object response. Status - {status}")


def uploadJsonToS3(data, key):
    response = client.put_object(
        Bucket='qufa-test', Key=key, Body=bytes(json.dumps(data).encode('UTF-8'))
    )
    status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")

    if status == 200:
        print(f"Successful S3 put_object response. Status - {status}")
    else:
        print(f"Unsuccessful S3 put_object response. Status - {status}")


def removeExtensionFromFilename(filename):
    tokens = filename.split('.')
    tokens.pop()

    newFilename = ".".join(tokens)
    return newFilename
